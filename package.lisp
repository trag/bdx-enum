;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :common-lisp-user)

(defpackage :bdx-enum
  (:use :common-lisp)
  (:documentation "A CL enumeration package. \
Includes bidirectional enumerators, products of infinite enumerators \
To be used to manipulate infinite (or big) sets or lists")
  (:export
   #:abstract-enumerator
   #:init-enumerator
   #:next-element-p
   #:next-element
   #:copy-enumerator
   #:call-enumerator
   #:list-enumerator
   #:make-append-enumerator
   #:make-list-enumerator
   #:make-vector-enumerator
   #:make-lazy-enumerator
   #:make-nary-product-enumerator
   #:make-binary-product-enumerator
   #:make-binary-diagonal-product-enumerator
   #:make-product-enumerator
   #:make-cartesian-enumerator
   #:make-enumerator-nil
   #:make-enumerator-cons
   #:make-enumerator-list
   #:make-mapping-enumerator
   #:make-funcall-enumerator
   #:make-apply-enumerator
   #:make-funcall-enumerator
   #:make-sequential-enumerator
   #:make-parallel-enumerator
   #:make-inductive-enumerator
   #:make-filter-enumerator
   #:make-no-duplicates-enumerator
   #:make-lazy-enumerator
   #:range-enumerator
   #:count-enum
   #:collect-enum
   #:collect-n-enum
   #:collect-enum-set
   #:make-flatten-enumerator
   #:make-k-combination-enumerator
   #:make-k-arrangements-enumerator
   #:make-one-value-enumerator
   #:make-cdr-enumerator
   #:make-nthcdr-enumerator
   #:make-empty-enumerator
   #:make-file-enumerator
   #:make-stream-enumerator))

