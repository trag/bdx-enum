;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(require :series)

(defparameter *l* (iota 50))
(time
 (defparameter
     *e*
   (series:collect-append
    (series:mapping
     ((item1 (series:scan *l*)))
     (series:collect
	 (series:mapping
	  ((item2 (series:scan *l*)))
	  (list item1 item2)))))))
