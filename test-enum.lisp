;;;; LICENSE: see trag/LICENSE.text
;;;; AUTHOR: Irène Durand

(in-package :common-lisp-user)

(defpackage :test-bdx-enum
  (:use :common-lisp :bdx-enum))

(in-package :test-bdx-enum)

(defun primep (n)
  (when (< n 2)
    (return-from primep nil))
  (when (or (= 2 n) (= 3 n))
    (return-from primep t))
  (when (evenp n)
    (return-from primep nil))
  (loop
    for d = 3 then (+ d 2)
    while (<= d (sqrt n))
    when (zerop (mod n d))
      do (return-from primep nil)
    finally (return t)))

(defparameter *primes*
  (make-filter-enumerator
   (make-inductive-enumerator 0 #'1+)
   #'primep))

(defparameter *p*
  (make-nary-product-enumerator
   (list
    (make-list-enumerator '(1 2 3))
    (make-list-enumerator '(a b)))))
