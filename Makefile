# Makefile pour projet Lisp

.SUFFIXES: .lisp

all:
	.PHONY clean clean-objs

clean:
	-rm -f *~

clean-objs:
	find . -name \*.fasl -print -exec rm \{\} \;
